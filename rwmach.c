/*
* This file is part of the following distribution (https://bitbucket.org/rodolfo_ferreira/projeto-compiladores).
* Copyright (c) 2015 Rodolfo Ferreira.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* For more informations, contact me through rodolfoandreferreira@gmail.com
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "list.h"
#include "rwmach.h"

#define STATISTICS
#define TURNER_LIST
//#define VALIDATOR

int const EXEC_SUCCESS = 0;
int const EXEC_IN_PROGRESS = 1;

#ifdef STATISTICS
int k_times = 0;
int s_times = 0;
int i_times = 0;
int b_times = 0;
int c_times = 0;
int slinha_times = 0;
int blinha_times = 0;
int clinha_times = 0;
#endif

void rewritemachine(char *input, node **list, int n) {
    int exec = EXEC_IN_PROGRESS;
    node **head = list;

#ifdef PROGRESS_TURNER
    int ntotal = (6 + (6 * n));
    printf("Progress:    %%");
    printf("\b");
    int pc_counter = 0;
#endif

#ifdef TURNER_LIST
    printf("Populating List\n");
    int begin = (int) clock();
#endif
    populateList(head, input);

#ifdef TURNER_LIST
    int end = (int) clock();
    int time_spent = (end - begin);
    int minutes = ((time_spent / (1000 * 60)) % 60);
    int seconds = (time_spent / 1000) % 60;
    int miliseconds = time_spent % 1000;
    printf("List populated in: %dmin %ds %dms\n", minutes, seconds, miliseconds);
#endif
    //Alternate between vectors
    while (exec == EXEC_IN_PROGRESS) {
        parse(list, &exec);
#ifdef VALIDATOR
        break;
#endif
    }

#ifdef STATISTICS
    printf("Rule K has been executed %d times\n", k_times);
    printf("Rule S has been executed %d times\n", s_times);
    printf("Rule I has been executed %d times\n", i_times);
    printf("Rule B has been executed %d times\n", b_times);
    printf("Rule C has been executed %d times\n", c_times);
    printf("Rule S' has been executed %d times\n", slinha_times);
    printf("Rule B' has been executed %d times\n", blinha_times);
    printf("Rule C' has been executed %d times\n\n", clinha_times);
#endif
}

void parse(node **head, int *exec) {

    node *current = *head;

    while (current->c == '@') {
        current = current->left;
    }

    char rule = current->c;

    switch (rule) {

        case 'K':
            parse_k(head, exec);
            break;

        case 'S':
            parse_s(head, exec);
            break;

        case 'I':
            parse_i(head, exec);
            break;

        case 'B':
            parse_b(head, exec);
            break;

        case 'C':
            parse_c(head, exec);
            break;

        case 'T': // S'
            parse_slinha(head, exec);
            break;

        case 'D': //B'
            parse_blinha(head, exec);
            break;

        case 'E': //C'
            parse_clinha(head, exec);
            break;

    }
}

// K case KCX -> C
void parse_k(node **head, int *exec) {
#ifdef STATISTICS
    k_times++;
#endif // STATISTICS

    // Finds the height of the tree
    int height = findHeight(head);

    // If there is not enough elements to perform the algorithm has reached a solution
    if (height < 2) {
        *exec = EXEC_SUCCESS;
    } else {

        //Build the node
        node *c_node = get_node(head, 1);
        node *newNode = malloc(sizeof(node_s));

        // If the tree only has its elements the head will point to it
        if (height == 2) {
            newNode->c = '@';
            newNode->left = c_node;
            newNode->right = NULL;
            *head = newNode;
            // otherwise will concat with the tail
        } else {
            node *current = *head;
            int counter = height - 3;
            while (counter > 0) {
                current = current->left;
                counter--;
            }
            current->left = c_node;
            current = NULL;
        }

        c_node = NULL;
        newNode = NULL;

    }
}


//S case
//SFGX -> (FX)(GX)
void parse_s(node **head, int *exec) {
#ifdef STATISTICS
    s_times++;
#endif // STATISTICS

    int height = findHeight(head);

    if (height < 3) {
        *exec = EXEC_SUCCESS;
    } else {
        node *f_node = get_node(head, 1);
        node *g_node = get_node(head, 2);
        node *x_node = get_node(head, 3);

        node *upperNode = malloc(sizeof(node_s));
        node *leftNode = malloc(sizeof(node_s));
        node *rightNode = malloc(sizeof(node_s));

        //@ -> fx
        leftNode->c = '@';
        leftNode->left = f_node;
        leftNode->right = x_node;

        //@ -> gx
        rightNode->c = '@';
        rightNode->left = g_node;
        rightNode->right = x_node;

        //@ <- @ -> @
        upperNode->c = '@';
        upperNode->left = leftNode;
        upperNode->right = rightNode;

        if (height == 3) {
            *head = upperNode;
        } else {
            node *current = *head;
            int counter = height - 4;
            while (counter > 0) {
                current = current->left;
                counter--;
            }
            current->left = upperNode;
            current = NULL;
        }

        f_node = NULL;
        g_node = NULL;
        x_node = NULL;
        upperNode = NULL;
        leftNode = NULL;
        rightNode = NULL;
    }
}

// I Case IX -> x
void parse_i(node **head, int *exec) {
#ifdef STATISTICS
    i_times++;
#endif // STATISTICS

    int height = findHeight(head);
    node *current = *head;

    //Checks if it has one upper node and if both children are filled (2 elements).
    if (height == 1 && current->right == NULL) {
        *exec = EXEC_SUCCESS;
    } else {
        node *x_node = get_node(head, 1);

        if (height == 1) {
            current->left = x_node;
            current->right = NULL;
        } else {
            int counter = height - 2;
            while (counter > 0) {
                current = current->left;
                counter--;
            }
            current->left = x_node;
            current = NULL;
        }

        x_node = NULL;
    }


}

//BFGX -> F(GX)
void parse_b(node **head, int *exec) {

#ifdef STATISTICS
    b_times++;
#endif // STATISTICS

    int height = findHeight(head);

    if (height < 3) {
        *exec = EXEC_SUCCESS;
    } else {
        node *f_node = get_node(head, 1);
        node *g_node = get_node(head, 2);
        node *x_node = get_node(head, 3);

        node *upperNode = malloc(sizeof(node_s));
        node *rightNode = malloc(sizeof(node_s));

        rightNode->c = '@';
        rightNode->left = g_node;
        rightNode->right = x_node;

        upperNode->c = '@';
        upperNode->left = f_node;
        upperNode->right = rightNode;

        if (height == 3) {
            *head = upperNode;
        } else {
            int counter = height - 4;
            node *current = *head;
            while (counter > 0) {
                current = current->left;
                counter--;
            }
            current->left = upperNode;
            current = NULL;
        }

        f_node = NULL;
        g_node = NULL;
        x_node = NULL;
        upperNode = NULL;
        rightNode = NULL;
    }

}

// CFGX -> FXG
void parse_c(node **head, int *exec) {
#ifdef STATISTICS
    c_times++;
#endif // STATISTICS
    int height = findHeight(head);

    if (height < 3) {
        *exec = EXEC_SUCCESS;
    } else {
        node *f_node = get_node(head, 1);
        node *g_node = get_node(head, 2);
        node *x_node = get_node(head, 3);

        node * upperNode = malloc(sizeof(node_s));
        node * rootNode = malloc(sizeof(node_s));

        rootNode->c = '@';
        rootNode->left = f_node;
        rootNode->right = x_node;

        upperNode->c = '@';
        upperNode->left = rootNode;
        upperNode->right = g_node;

        if (height == 3) {
            *head = upperNode;
        } else {
            int counter = height - 4;
            node *current = *head;
            while (counter > 0) {
                current = current->left;
                counter--;
            }
            current->left = upperNode;
            current = NULL;
        }

        f_node = NULL;
        g_node = NULL;
        x_node = NULL;

        upperNode = NULL;
        rootNode = NULL;

    }
}


// S' = T
// TCFGX = C(FX)(GX)
void parse_slinha(node **head, int *exec) {
#ifdef STATISTICS
    slinha_times++;
#endif // STATISTICS

    int heigh = findHeight(head);

    if (heigh < 4) {
        *exec = EXEC_SUCCESS;
    } else {
        node *c_node = get_node(head, 1);
        node *f_node = get_node(head, 2);
        node *g_node = get_node(head, 3);
        node *x_node = get_node(head, 4);

        node *upperNode = malloc(sizeof(node_s));
        node *leftNode = malloc(sizeof(node_s));
        node *rightNode = malloc(sizeof(node_s));
        node *innerNode = malloc(sizeof(node_s));

        innerNode->c = '@';
        innerNode->left = f_node;
        innerNode->right = x_node;

        leftNode->c = '@';
        leftNode->left = c_node;
        leftNode->right = innerNode;

        rightNode->c = '@';
        rightNode->left = g_node;
        rightNode->right = x_node;

        upperNode->c = '@';
        upperNode->left = leftNode;
        upperNode->right = rightNode;

        node *current = *head;
        if (heigh == 4) {
            *head = upperNode;
        } else {
            int counter = heigh - 5;
            while (counter > 0) {
                current = current->left;
                counter--;
            }
            current->left = upperNode;
            current = NULL;
        }

        c_node = NULL;
        f_node = NULL;
        g_node = NULL;
        x_node = NULL;

        innerNode = NULL;
        leftNode = NULL;
        rightNode = NULL;
        upperNode = NULL;
    }

}

// B' = D
// DCFGX = CF(GX)
void parse_blinha(node **head, int *exec) {
#ifdef STATISTICS
    blinha_times++;
#endif // STATISTICS

    int height = findHeight(head);

    if (height < 4) {
        *exec = EXEC_SUCCESS;
    } else {
        node *c_node = get_node(head, 1);
        node *f_node = get_node(head, 2);
        node *g_node = get_node(head, 3);
        node *x_node = get_node(head, 4);

        node *upperNode = malloc(sizeof(node_s));
        node *leftNode = malloc(sizeof(node_s));
        node *rightNode = malloc(sizeof(node_s));

        leftNode->c = '@';
        leftNode->left = c_node;
        leftNode->right = f_node;

        rightNode->c = '@';
        rightNode->left = g_node;
        rightNode->right = x_node;

        upperNode->c = '@';
        upperNode->left = leftNode;
        upperNode->right = rightNode;

        if (height == 4) {
            *head = upperNode;
        } else {
            int counter = height - 5;
            node * current = *head;
            while (counter > 0) {
                current = current->left;
                counter--;
            }
            current->left = upperNode;
            current = NULL;
        }

        c_node = NULL;
        f_node = NULL;
        g_node = NULL;
        x_node = NULL;

        leftNode = NULL;
        rightNode = NULL;
        upperNode = NULL;

    }
}

//C' = E
// ECFGX = C(FX)G
void parse_clinha(node **head, int *exec) {

#ifdef STATISTICS
    clinha_times++;
#endif // STATISTICS

    int height = findHeight(head);

    if (height < 4) {
        *exec = EXEC_SUCCESS;
    } else {
        node *c_node = get_node(head, 1);
        node *f_node = get_node(head, 2);
        node *g_node = get_node(head, 3);
        node *x_node = get_node(head, 4);

        node *upperNode = malloc(sizeof(node_s));
        node *rightNode = malloc(sizeof(node_s));
        node *root = malloc(sizeof(node_s));

        rightNode->c = '@';
        rightNode->left = f_node;
        rightNode->right = x_node;

        root->c = '@';
        root->left = c_node;
        root->right = rightNode;

        upperNode->c = '@';
        upperNode->left = root;
        upperNode->right = g_node;

        if (height == 4) {
            *head = upperNode;
        } else {
            int counter = height - 5;
            node * current = *head;
            while (counter > 0) {
                current = current->left;
                counter--;
            }
            current->left = upperNode;
            current = NULL;
        }

        c_node = NULL;
        f_node = NULL;
        g_node = NULL;
        x_node = NULL;

        root = NULL;
        rightNode = NULL;
        upperNode = NULL;
    }

}

void populateList(node **head, char *input) {
    int index = 0;
    int size = (int) strlen(input);
    populate(head, input, &index, &size);

}

void populate(node **head, char *input, int *index, int *size) {
    while (*index < *size) {
        char c = input[*index];
        *index = *index + 1;
        if (c != '(' && c != ')') {
            node *newElemen = malloc(sizeof(node_s));
            newElemen->c = c;
            newElemen->right = NULL;
            newElemen->left = NULL;
            add_node(head, newElemen);
        } else if (c == '(') {
            node *list = malloc(sizeof(node_s));
            list->c = '@';
            list->left = NULL;
            list->right = NULL;
            populate(&list, input, index, size);
            add_node(head, list);
        } else if (c == ')') {
            break;
        }
    }
}

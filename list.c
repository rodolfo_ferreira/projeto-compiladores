/*
* This file is part of the following distribution (https://bitbucket.org/rodolfo_ferreira/projeto-compiladores).
* Copyright (c) 2015 Rodolfo Ferreira.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* For more informations, contact me through rodolfoandreferreira@gmail.com
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

node **create_list() {
    node **holder = malloc(sizeof(node_s));
    node *head = malloc(sizeof(node_s));

    head->c = '@';
    head->left = NULL;
    head->right = NULL;

    *holder = head;

    return holder;
}

int add_node(node **head, node *ptr) {
    node *current = *head;

    if (current->left == NULL && current->right == NULL) {
        current->left = ptr;
    } else if (current->right == NULL) {
        current->right = ptr;
    } else {

        node *newNode = malloc(sizeof(node_s));
        newNode->c = '@';
        newNode->left = current;
        newNode->right = ptr;

        *head = newNode;
    }

    return current->c;
}

int pop_node(node **head) {
    char rem_elem = '\0';
    node *current = *head;

    while ((current->left->left != NULL) && current->left->left->c == '@') {
        current = current->left;
    }

    if (current->left->left == NULL || current->left->right == NULL) {
        current->left = current->right;
        current->right = NULL;
    } else {
        node *temp = current->left;
        current->left = current->left->right;
        rem_elem = current->c;
    }

    return rem_elem;
}

void sizeOf(node *head, int *result) {
    node *current = head;
    while (current != NULL) {
        if (current->c == '@') {
            if (current->right != NULL && current->right->c == '@') {
                sizeOf(current->right, result);
            } else {
                *result = *result + 1;
            }
        } else if (current->c != '@') {
            *result = *result + 1;
        }
        current = current->left;
    }
}

int findSize(node **head) {
    int result = 0;
    node *current = *head;
    sizeOf(current, &result);
    return result;
}

int findHeight(node **head) {
    int result = 0;
    node *current = *head;
    while (current != NULL) {
        if (current->c == '@')
        result++;
        current = current->left;
    }
    return result;
}

node *get_node(node **head, int index) {
    node *result;
    int size = findHeight(head);
    int down = size - index;

    node *current = *head;
    for (int i = 0; i < down - 0 && current->left->c == '@'; i++) {
        if (current->left == NULL) {
            printf("Array index out of bounds");
            exit(EXIT_FAILURE);
        }

        current = current->left;
    }

    if (index == 0) {
        result = current->left;
    } else {
        result = current->right;
    }
    return result;
}

void print_list(node **head) {
    node *current = *head;
    print(current);
    printf("\n");

}

void print(node *head) {
    node *current = head;
    if (current != NULL) {
        if (current->c != '@') {
            printf("%c", current->c);
        } else {
            print(current->left);
            if (current->right != NULL && current->right->c == '@') {
                printf("(");
            }
            print(current->right);
            if (current->right != NULL && current->right->c == '@') {
                printf(")");
            }
            current = current->left;
        }

    }
}


/*
* This file is part of the following distribution (https://bitbucket.org/rodolfo_ferreira/projeto-compiladores).
* Copyright (c) 2015 Rodolfo Ferreira.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* For more informations, contact me through rodolfoandreferreira@gmail.com
*/

/*
 * File:   rwmach.h
 * Author: Rodolfo Andr� Barbosa Ferreira
 *
 * Created on 10 de Maio de 2017, 11:02
 */

#include "list.h"

#ifndef RWMACH_H
#define RWMACH_H
void rewritemachine(char * input, node ** list, int n);
void parse(node ** head, int *exec);
void parse_k(node ** head, int *exec);
void parse_s(node ** head, int *exec);
void parse_i(node ** head, int *exec);
void parse_b(node ** head, int *exec);
void parse_c(node ** head, int *exec);
void parse_slinha(node ** head, int *exec);
void parse_blinha(node ** head, int *exec);
void parse_clinha(node ** head, int *exec);
void populateList(node **head, char *input);
void populate(node ** head, char * input, int * size, int * index);
#ifdef __cplusplus
extern "C" {
#endif




#ifdef __cplusplus
}
#endif

#endif /* RWMACH_H */


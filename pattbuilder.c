/*
* This file is part of the following distribution (https://bitbucket.org/rodolfo_ferreira/projeto-compiladores).
* Copyright (c) 2015 Rodolfo Ferreira.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* For more informations, contact me through rodolfoandreferreira@gmail.com
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pattbuilder.h"

char * build_pattern(int n, char *input) {
	char y[] = "T(EBI(SII))I(B(EBI(SII))I)";
	int y_len = 26;
	int pattern_length = 0;
	int pattern_size = 6;
	if (n > 0) {
		pattern_size += 6 * (n - 1);
	}
	char *pattern = (char*)malloc(pattern_size * sizeof(char*)); //  Aloca��o do padr�o (ATEN��O TAMANHO)
	//int input_size = ((pattern_size - n) + (n*y_len) + 1) * n * n;
	//char *input = (char*) malloc(input_size * sizeof(char)); // Aloca��o de entrada (ATEN��O TAMANHO)



	for (int i = 0; i < n; i++) {
		pattern[pattern_length++] = 'Y';
		pattern[pattern_length++] = '(';
		pattern[pattern_length++] = '(';
	}
	pattern[pattern_length++] = 'Y';
	pattern[pattern_length++] = '(';
	pattern[pattern_length++] = 'K';
	pattern[pattern_length++] = 'K';
	pattern[pattern_length++] = ')';

	for (int i = 0; i < n; i++) {
		pattern[pattern_length++] = ')';
		pattern[pattern_length++] = 'K';
		pattern[pattern_length++] = ')';
	}

	pattern[pattern_length++] = '\0';


	int i_length = 0;
	for (int i = 0; i < pattern_length; i++) {
		char cur_char = pattern[i];
		if (cur_char == 'Y') {
			for (int j = 0; j < y_len; j++) {
				input[i_length++] = y[j];
			}
		}
		else {
			input[i_length++] = cur_char;
		}
	}
	input[i_length] = '\0';
	free(pattern);
	pattern = NULL;
	return input;

}



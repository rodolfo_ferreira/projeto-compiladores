/*
* This file is part of the following distribution (https://bitbucket.org/rodolfo_ferreira/projeto-compiladores).
* Copyright (c) 2015 Rodolfo Ferreira.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* For more informations, contact me through rodolfoandreferreira@gmail.com
*/

#ifndef LIST_H
#define LIST_H
struct node {
	char c;
	struct node *left;
	struct node *right;
} node_s;

typedef struct node node;

void print_list(node ** head);
void print(node * head);
int add_node(node ** head, node * ptr);
int pop_node(node ** head);
int findSize(node ** head);
int findHeight(node **head);
node * get_node(node ** head, int index);
node ** create_list();
#ifdef __cplusplus
extern "C" {
#endif




#ifdef __cplusplus
}
#endif

#endif /* LIST_H */
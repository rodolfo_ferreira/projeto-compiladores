/*
* This file is part of the following distribution (https://bitbucket.org/rodolfo_ferreira/projeto-compiladores).
* Copyright (c) 2015 Rodolfo Ferreira.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* For more informations, contact me through rodolfoandreferreira@gmail.com
*/

/*
* File:   pattbuilder.h
* Author: Rodolfo Andr� Barbosa Ferreira
*
* Created on 10 de Maio de 2017, 11:02
*/

#ifndef PATTBUILDER_H
#define PATTBUILDER_H
char* build_pattern(int n, char *input);
#ifdef __cplusplus
extern "C" {
#endif




#ifdef __cplusplus
}
#endif

#endif /* PATTBUILDER_H */
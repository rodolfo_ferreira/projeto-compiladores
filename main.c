/*
* This file is part of the following distribution (https://bitbucket.org/rodolfo_ferreira/projeto-compiladores).
* Copyright (c) 2015 Rodolfo Ferreira.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* For more informations, contact me through rodolfoandreferreira@gmail.com
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "rwmach.h"
#include "pattbuilder.h"

#define STATISTICS

char input[100000000];

int main() {
    int n = 1000;
    build_pattern(n, input);
    printf("\n");
#ifdef STATISTICS
    int begin = (int) clock();
#endif
    node **list = create_list();
    rewritemachine(input, list, n);

    printf("Result: ");
    print_list(list);

#ifdef STATISTICS
    int end = (int) clock();
    int time_spent = (end - begin);

    int minutes = ((time_spent / (1000 * 60)) % 60);
    int seconds = (time_spent / 1000) % 60;
    int miliseconds = time_spent % 1000;

    printf("Executed in: %dmin %ds %dms\n", minutes, seconds, miliseconds);
#endif

    system("pause");
    return EXIT_SUCCESS;
}